clc;
clear;
close all;

%% Genera un grafica de Control
% Comienza la Localizacion
x_start = [0.8, -0.5];

% Diseña variables en locales puntos
i1 = -1.0:0.01:1.0;
i2 = -1.0:0.01:1.0;
[x1m, x2m] = meshgrid(i1, i2);
fm = 0.2 + x1m.^2 + x2m.^2 - 0.1*cos(6.0*pi*x1m) - 0.1*cos(6.0*pi*x2m);

% Grafica de Contorno
fig = figure(1);
[C,h] = contour(x1m,x2m,fm);
clabel(C,h,'Labelspacing',250);
title('Recogida Simulada');
xlabel('x1');
ylabel('x2');
hold on;

%% Algoritmo de Recogida Simulada

% Numero de Ciclos
n = 50;
% Numero de Intentos por ciclos
m = 50;
% Numero de soluciones aceptadas
na = 0.0;
% Probabilidad de aceptar la peor solucion en el inicio
p1 = 0.7;
% Probabilidad de acetar la peor solucion en el final
p50 = 0.001;
% Temperatura Inicial
t1 = -1.0/log(p1);
% Temperatura Final
t50 = -1.0/log(p50);
% Reduccion fraccional de todos los ciclos
frac = (t50/t1)^(1.0/(n-1.0));
% Inicializando X
x = zeros(n+1,2);
x(1,:) = x_start;
xi = x_start;
na = na + 1.0;
% La mejor solucion hasta aqui
xc = x(1,:);
fc = f(xi);
fs = zeros(n+1,1);
fs(1,:) = fc;
% Actual Temperatura
t = t1;
% Promedio DeltaE
DeltaE_avg = 0.0;
for i=1:n
    disp(['Ciclo: ',num2str(i),' con Una Temperatura de: ',num2str(t)])
    for j=1:m
        % Generando un nuevo intento de Puntos
        xi(1) = xc(1) + rand() - 0.5;
        xi(2) = xc(2) + rand() - 0.5;
        % Establiendo limites maximos y minimos
        xi(1) = max(min(xi(1),1.0),-1.0);
        xi(2) = max(min(xi(2),1.0),-1.0);
        DeltaE = abs(f(xi)-fc);
        if (f(xi)>fc)
            %             % Inicializando DeltaE si una mala solucion fue inicializada
            %             %   en la primera iteracion
            if (i==1 && j==1)
                DeltaE_avg = DeltaE;
            end
            % Funcion objetivoes mala
            % Generando una probabilidad de aceptacion
            p = exp(-DeltaE/(DeltaE_avg * t));
            %             % Determinar el peor clima para establecer el peor punto
            if (rand()<p)
                % Aceptando la peor solucion
                accept = true;
            else
                % No aceptando la peor solucion
                accept = false;
            end
        else
            % La funcion objetivo es baja, aceptada automaticamente
            accept = true;
        end
        if (accept==true)
            % Actualizando actualmente la funcion aceptada
            xc(1) = xi(1);
            xc(2) = xi(2);
            fc = f(xc);
            % incrementeando el numero de soluciones aceptadas
            na = na + 1.0;
            % Actualizando DeltaE_avg
            DeltaE_avg = (DeltaE_avg * (na-1.0) +  DeltaE) / na;
        end
    end
    % Recuperando el mejor valor x en el fin de cada ciclo
    x(i+1,1) = xc(1);
    x(i+1,2) = xc(2);
    fs(i+1) = fc;
    % Baja la temperatura para cada ciclo
    t = frac * t;
end
% Imprime la mejor solucion
disp(['Mejor Solucion: ',num2str(xc)])
disp(['Mejor Objetivo: ',num2str(fc)])
plot(x(:,1),x(:,2),'r-o')
saveas(fig,'control','png')

fig = figure(2);
subplot(2,1,1)
plot(fs,'r.-')
legend('Objetivo')
subplot(2,1,2)
hold on
plot(x(:,1),'b.-')
plot(x(:,2),'g.-')
legend('x_1','x_2')

% Guarda la figura como un PNG
saveas(fig,'iteraciones','png')
