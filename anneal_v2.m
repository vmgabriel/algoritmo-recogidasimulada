clc;
clear;
close all;

%% Generando el contorno
% Localizacion inicial
x_start = [0.8, -0.5];

% Diseñando variables en el punto base
i1 = -1.0:0.01:1.0;
i2 = -1.0:0.01:1.0;
[x1m, x2m] = meshgrid(i1, i2);
fm = 0.2 + x1m.^2 + x2m.^2 - 0.1*cos(6.0*pi*x1m) - 0.1*cos(6.0*pi*x2m);

% Imprimiendo contorno
fig = figure(1);
[C,h] = contour(x1m,x2m,fm);
clabel(C,h,'Labelspacing',250);
title('Algoritmo de Recogida Simulada');
xlabel('x1');
ylabel('x2');
hold on;

%% Algoritmo de Recogida simulada

% Numero de Ciclos
n = 50;
% Numero de intentos por ciclo
m = 50;
% Numero de solucones aceptadas
na = 0.0;
% Probabilidad de Seleccionar la peor solucion en el inicio
p1 = 0.7;
% Probabilidad de Aceptar la peor solucion en el final
p50 = 0.001;
% Temperatura Inicial
t1 = -1.0/log(p1);
% temperatura Final
t50 = -1.0/log(p50);
% Reduccion fraccional de todos los ciclos
frac = (t50/t1)^(1.0/(n-1.0));
% Inicializando X
x = zeros(n+1,2);
x(1,:) = x_start;
xi = x_start;
na = na + 1.0;
% Actual mejor resultado
% xc = x(1,:);
fc = f(xi);
fs = zeros(n+1,1);
fs(1,:) = fc;
% Actual Temperatura
t = t1;
% DeltaE Promedio
DeltaE_avg = 0.0;
for i=1:n
    disp(['Ciclo: ',num2str(i),' con Temperatura: ',num2str(t)])
    xc(1) = x(i,1);
    xc(2) = x(i,2);
    for j=1:m
        % Generando un nuevo intento de puntos
        xi(1) = xc(1) + rand() - 0.5;
        xi(2) = xc(2) + rand() - 0.5;
        % Limitando a maximo y minimo
        xi(1) = max(min(xi(1),1.0),-1.0);
        xi(2) = max(min(xi(2),1.0),-1.0);
        DeltaE = abs(f(xi)-fc);
        if (f(xi)>fc)
        %             % Inicializando DeltaE si una mala solucion fue inicializada
        %             %   en la primera iteracion
            if (i==1 && j==1)
                DeltaE_avg = DeltaE;
            end
            % Funcion Objetivo mas baja
            % Generando probabilidad de Aceptacion
            p = exp(-DeltaE/(DeltaE_avg * t));
            %             % Determinando el clima para determinar el DELTA
            if (rand()<p)
                % Aceptando la peor solucion
                accept = true;
            else
                % No aceptando la peor solucion
                accept = false;
            end
        else
            % Funcion Objetivo es Baja, Aceptando automaticamente
            accept = true;
        end
%        disp(['Cycle: ',num2str(i),' trial: ',num2str(j)]);
%        accept
        if (accept==true)
            % Actualizando actualment las soluciones aceptadas
%            disp(['Cycle i:', num2str(i)]);
%            disp(['Trail j:', num2str(j)]);
            xc(1) = xi(1);
%            fprintf('xc(1) (in %s. cycle, %s. trial): %s \n',...
%                num2str(i),num2str(j),num2str(xc(1)));
            xc(2) = xi(2);
%            fprintf('xc(2) (in %s. cycle, %s. trial): %s \n',...
%                num2str(i),num2str(j),num2str(xc(2)));
            fc = f(xc);
%            fprintf('fc    (in %s. cycle, %s. trial): %s \n',...
%                num2str(i),num2str(j),num2str(fc));
            % Poniendo ACEPTADOS candidatos xc y soluciones correspondientes a f(xc)
            xa(j,1) = xc(1);
            xa(j,2) = xc(2);
            fa(j) = f(xc);
            % Incrementando numero de soluciones aceptadas
            na = na + 1.0;
            % Actualizando DeltaE_avg
            DeltaE_avg = (DeltaE_avg * (na-1.0) +  DeltaE) / na;
        else
            fa(j) = 0.0;
        end
    end
    fa_Min_Index = find(nonzeros(fa) == min(nonzeros(fa)));
    if isempty(fa_Min_Index) == 0
        x(i+1,1) = xa(fa_Min_Index,1);
        x(i+1,2) = xa(fa_Min_Index,2);
        fs(i+1)  = fa(fa_Min_Index);
    else
        x(i+1,1) = x(i,1);
        x(i+1,2) = x(i,2);
        fs(i+1)  = fs(i);
    end
    % Bajando la temperatura para el siguiente ciclo
    t = frac * t;
    fa = 0.0;
end
% Imprime Soluciones
disp(['Mejor Candidato: ',num2str(xc)])
disp(['Mejor Solucion: ',num2str(fc)])
plot(x(:,1),x(:,2),'r-o')
saveas(fig,'contorno','png')

fig = figure(2);
subplot(2,1,1)
plot(fs,'r.-')
legend('Objectivo')
subplot(2,1,2)
hold on
plot(x(:,1),'b.-')
plot(x(:,2),'g.-')
legend('x_1','x_2')

% Guardando imagen PNG
saveas(fig,'iteraciones','png')
