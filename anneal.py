

# Import some other libraries that we'll need
# matplotlib and numpy packages must also be installed

import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import random
import math

# Definiendo la funcion Objetivo, ver la documentacion
def f(x):
    x1 = x[0]
    x2 = x[1]
    obj = 0.2 + x1**2 + x2**2 - 0.1*math.cos(6.0*3.1415*x1) - 0.1*math.cos(6.0*3.1415*x2)
    return obj

# Loacalizacion Inicial
x_start = [0.8, -0.5]

# Diseñando variables en el punto de estado
i1 = np.arange(-1.0, 1.0, 0.01)
i2 = np.arange(-1.0, 1.0, 0.01)
x1m, x2m = np.meshgrid(i1, i2)
fm = np.zeros(x1m.shape)
for i in range(x1m.shape[0]):
    for j in range(x1m.shape[1]):
        fm[i][j] = 0.2 + x1m[i][j]**2 + x2m[i][j]**2 \
             - 0.1*math.cos(6.0*3.1415*x1m[i][j]) \
             - 0.1*math.cos(6.0*3.1415*x2m[i][j])

# Creando punto de control
plt.figure()
# Especificando las lineas de control
# lines = range(2,52,2)
# Imprime Controles
CS = plt.contour(x1m, x2m, fm)#,lines)
# Texto de Control
plt.clabel(CS, inline=1, fontsize=10)
# Agrega mas texto a la funcion
plt.title('Funcion no Lineal')
plt.xlabel('x1')
plt.ylabel('x2')

##################################################
# Algoritmo de Regocida Simulada
##################################################
# Numero de Ciclos
n = 50
# Numero de Intentos por Ciclo
m = 50
# Numero de soluciones aceptadas
na = 0.0
# Probabilidad de aceptar la peor solucion al principio
p1 = 0.7
# Probavilidad de acpetar la peor solucion al final
p50 = 0.001
# Temperatura Inicial
t1 = -1.0/math.log(p1)
# Temperatura Final
t50 = -1.0/math.log(p50)
# Reduccion fraccional de cada uno de los ciclos
frac = (t50/t1)**(1.0/(n-1.0))
# Inicializando X
x = np.zeros((n+1,2))
x[0] = x_start
xi = np.zeros(2)
xi = x_start
na = na + 1.0
# Actual mejor resultado hasta el momento
xc = np.zeros(2)
xc = x[0]
fc = f(xi)
fs = np.zeros(n+1)
fs[0] = fc
# Actual Temperatura
t = t1
# DeltaE Average
DeltaE_avg = 0.0
for i in range(n):
    print('Ciclo: ' + str(i) + ' Con Temperatura: ' + str(t))
    for j in range(m):
        # Generando un nuevo intento de puntos
        xi[0] = xc[0] + random.random() - 0.5
        xi[1] = xc[1] + random.random() - 0.5
        # Establece Limites de Minimo y Maximo
        xi[0] = max(min(xi[0],1.0),-1.0)
        xi[1] = max(min(xi[1],1.0),-1.0)
        DeltaE = abs(f(xi)-fc)
        if (f(xi)>fc):
            # Iniciando DeltaE_avg if de la peor solucion que fue encontrada
            #   en la primera iteracion
            if (i==0 and j==0): DeltaE_avg = DeltaE
            # Funcion Objetivo es baja
            # Generando probabilidad de aceptacion
            p = math.exp(-DeltaE/(DeltaE_avg * t))
            # Determinando clima para ver el resultado
            if (random.random()<p):
                # Aceptando la peor solucion
                accept = True
            else:
                # No aceptando la peor solucion
                accept = False
        else:
            # Funcion Objetivo es baja, Aceptando automaticamente
            accept = True
        if (accept==True):
            # Aceptando las soluciones
            xc[0] = xi[0]
            xc[1] = xi[1]
            fc = f(xc)
            # Incrementando el numero de soluciones aceptadas
            na = na + 1.0
            # Actualizando DeltaE_avg
            DeltaE_avg = (DeltaE_avg * (na-1.0) +  DeltaE) / na
    # Guardando la solucion para cada uno de los X
    x[i+1][0] = xc[0]
    x[i+1][1] = xc[1]
    fs[i+1] = fc
    # Bajando la temperatura en cada uno de los ciclos
    t = frac * t

# Imprimiendo Solucion
print('Best solution: ' + str(xc))
print('Best objective: ' + str(fc))

plt.plot(x[:,0],x[:,1],'y-o')
plt.savefig('control.png')

fig = plt.figure()
ax1 = fig.add_subplot(211)
ax1.plot(fs,'r.-')
ax1.legend(['Objetivo'])
ax2 = fig.add_subplot(212)
ax2.plot(x[:,0],'b.-')
ax2.plot(x[:,1],'g--')
ax2.legend(['x1','x2'])

# Guardando la imagen como un PNG
plt.savefig('iteraciones.png')

plt.show()
